

let data = `{
	"Users":[
	{"UserID" : "00A1",
	"FirstName" : "Mauro",
	"LastName" : "Munoz",
	"Email" : "mauro@gmail.com",
	"Password" : "user123",
	"IsAdmin" : false,
	"MobileNumber" : "0912312523"},

	{"UserID" : "00A2",
	"FirstName" : "Mark",
	"LastName" : "Munoz",
	"Email" : "mark@gmail.com",
	"Password" : "user123",
	"IsAdmin" : false,
	"MobileNumber" : "09123125233"}
			],

	"Order":[
	{"OrderID" : "OR001",
	"User_Id" : "00A1",
	"TransactionDate" : "2022/03/02 2:32",
	"Status" : "InCart",
	"Total" : 1360},

	{"OrderID" : "OR002",
	"User_Id" : "00A1",
	"TransactionDate" : "2022/03/02 2:35",
	"Status" : "InCart",
	"Total" : 60}
			],

	"Product": [
	{"ProductID": "PRD001",
	"Name" : "Fiction Book",
	"Description" : "Starwars",
	"Price" : 500,
	"Stocks" : 5,
	"IsActive" : true,
	"SKU" : "00001"},

	{"ProductID": "PRD002",
	"Name" : "Weapon",
	"Description" : "Sword",
	"Price" : 300,
	"Stocks" : 5,
	"IsActive" : true,
	"SKU" : "00002"},

	{"ProductID": "PRD003",
	"Name" : "Accessory",
	"Description" : "Clip",
	"Price" : 30,
	"Stocks" : 5,
	"IsActive" : true,
	"SKU" : "00003"}
				],

	"OrderProduct":[
	{"OrderProductsID":"ORPR001",
	"OrderID":"OR001",
	"ProductID":"PRD001",
	"Quantity":2,
	"Price":500,
	"Subtotal": 1000},

	{"OrderProductsID":"ORPR002",
	"OrderID":"OR001",
	"ProductID":"PRD002",
	"Quantity":1,
	"Price":300,
	"Subtotal": 300},

	{"OrderProductsID":"ORPR003",
	"OrderID":"OR001",
	"ProductID":"PRD003",
	"Quantity":2,
	"Price":30,
	"Subtotal": 60},

	{"OrderProductsID":"ORPR004",
	"OrderID":"OR002",
	"ProductID":"PRD003",
	"Quantity":2,
	"Price":30,
	"Subtotal": 60}
]
}`

console.log(JSON.parse(data))